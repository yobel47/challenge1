const checkTypeNumber = (givenNumber) => {
	if(typeof(givenNumber)=="number"){
		if(givenNumber%2==0){
			return "GENAP"
		}else{
			return "GANJIL"
		}
	}else if(givenNumber==null){
		return "Error: Bro where is the parameter?"
	}else{
		return "Error: Invalid data type"
	}
}

// EXPECTED RESULT
// Ketika function tersebut dipanggil

console.log(checkTypeNumber(10)) //OUTPUT yang keluar => "GENAP"
console.log(checkTypeNumber(3)) //OUTPUT yang keluar => "GANJIL"
console.log(checkTypeNumber("3")) //OUTPUT yang keluar => "Error: Invalid data type"
console.log(checkTypeNumber({})) //OUTPUT yang keluar => "Error: Invalid data type"
console.log(checkTypeNumber([])) //OUTPUT yang keluar => "Error: Invalid data type"
console.log(checkTypeNumber()) //OUTPUT yang keluar => "Error: Bro where is the parameter"