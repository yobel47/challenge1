function isValidPassword(email){
	if(typeof(email)=="string"){
      if(/^(?=.*[a-z])(?=.*[0-9])(?=.{8,})/.test(email)){
		return true
      }else{
        return false
      }
  }else if(email==null){
    return "Error: Bro where is the parameter?"
  }else{
    return "Error: Invalid data type"
  }
}

//EXPECTED RESULT
//Ketika function tersebut dipanggil
console.log(isValidPassword('Meong2021'))
//EXPECTED OUTPUT => true
//Karena memenuhi requirement, Meong2021 terdiri dari 8 huruf, ada huruf besar, ada huruf kecil, dan ada angka

console.log(isValidPassword('MEONG2021'))
// // //EXPECTED OUTPUT => false (Karena meong 2021 tidak ada huruf besar)

console.log(isValidPassword('@eong'))
// // //EXPECTED OUTPUT => false (Karena @eong tidak ada angka dan terdiri dari hanya 5 huruf)

console.log(isValidPassword('Meongoooo'))
// // //EXPECTED OUTPUT => false (Karena Meong2 hanya 6 huruf)

console.log(isValidPassword(0))
// // //EXPECTED OUTPUT => Error: Invalid data type

console.log(isValidPassword())
// EXPECTED OUTPUT => Error: Bro where is the parameter?