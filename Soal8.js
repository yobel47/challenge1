const dataPenjualanNovel = [
    {
      idProduct: 'BOOK002421',
      namaProduk: 'Pulang - Pergi',
      penulis: 'Tere Liye',
      hargaBeli: 60000,
      hargaJual: 86000,
      totalTerjual: 150,
      sisaStok: 17,
    },
    {
      idProduct: 'BOOK002351',
      namaProduk: 'Selamat Tinggal',
      penulis: 'Tere Liye',
      hargaBeli: 75000,
      hargaJual: 103000,
      totalTerjual: 171,
      sisaStok: 20,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Garis Waktu',
      penulis: 'Fiersa Besari',
      hargaBeli: 67000,
      hargaJual: 99000,
      totalTerjual: 213,
      sisaStok: 5,
    },
    {
      idProduct: 'BOOK002941',
      namaProduk: 'Laskar Pelangi',
      penulis: 'Andrea Hirata',
      hargaBeli: 55000,
      hargaJual: 68000,
      totalTerjual: 20,
      sisaStok: 56,
    },
  ];
  
  function getInfoPenjualan(dataPenjualan){
    if(Array.isArray(dataPenjualan)){
      for(let o=0;o<dataPenjualan.length;o++){
        if(typeof(dataPenjualan)=="object"){
          let totalKeuntungan = 0
          let totalModal = 0
          let bukuTerlaris = 0
          let indexBuku = 0
          let penulisTerlaris = cekPenulis(dataPenjualan)
          for(let i=0;i<dataPenjualan.length;i++){
            totalKeuntungan += ((dataPenjualan[i].hargaJual - dataPenjualan[i].hargaBeli) * dataPenjualan[i].totalTerjual)
            totalModal += ((dataPenjualanNovel[i].totalTerjual + dataPenjualanNovel[i].sisaStok) * dataPenjualanNovel[i].hargaBeli)
            if(bukuTerlaris<dataPenjualanNovel[i].totalTerjual){
              bukuTerlaris = dataPenjualanNovel[i].totalTerjual
              indexBuku++ 
            }
          }
          let presentaseKeuntungan = totalKeuntungan / totalModal * 100
          return {
            totalKeuntungan: totalKeuntungan.toLocaleString("id-ID", {style:"currency", currency:"IDR"}),
            totalModal: totalModal.toLocaleString("id-ID", {style:"currency", currency:"IDR"}),
            presentaseKeuntungan: presentaseKeuntungan.toFixed(2)+"%",
            produkBukuTerlaris: dataPenjualan[indexBuku-1].namaProduk,
            penulisBukuTerlaris: penulisTerlaris
          }
        }
      }
        
      }else if(dataPenjualan==null){
        return "Error: Bro where is the parameter?"
      }else{
        return "Error: Invalid data type"
      }
  }
  
  function cekPenulis(dataPenjualan){
      let dataPenulis = Object.values(dataPenjualan.reduce(
        (dataPenulis, { penulis, totalTerjual }) => {
          dataPenulis[penulis] ??= { penulis, totalTerjual: 0 };
          dataPenulis[penulis].totalTerjual += totalTerjual;
          return dataPenulis;
      }, {}));
    let maxTerjual = Math.max.apply(Math, dataPenulis.map(function(o) { return o.totalTerjual }))
    for(let i=0;i<dataPenulis.length;i++){
        if(dataPenulis[i].totalTerjual==maxTerjual){
        return dataPenulis[i].penulis
      }
    }
  }
  
  console.log(getInfoPenjualan(dataPenjualanNovel))
  //EXPECTED RESULT => OBJECT dengan format seperti yang disebutkan diatas