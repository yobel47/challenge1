function checkEmail(email){
  if(typeof(email)=="string"){
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)){
      return "VALID"
    }else if(!(/@/.test(email))){
      return "ERROR: Need @ / Not an email"    
    }else{
      return "INVALID"    
    }
  }else if(email==null){
    return "Error: Bro where is the parameter?"
  }else{
    return "Error: Invalid data type"
  }
}

// EXPECTED RESULT
// Ketika function tersebut dipanggil
console.log(checkEmail('apranata@binar.co.id')) //OUTPUT yang keluar => "VALID"
console.log(checkEmail('apranata@binar.com')) //OUTPUT yang keluar => "VALID"
console.log(checkEmail('apranata@binar')) //OUTPUT yang keluar => "INVALID"
console.log(checkEmail('apranata')) //Error: INVALID
console.log(checkEmail(3322)) //Error: Invalid data type
console.log(checkEmail(111)) //Error: Bro where is the parameter?