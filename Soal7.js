const dataPenjualanPakAldi = [
  {
    namaProduct : 'Sepatu Futsal Nike Vapor Academy 8',
    hargaSatuan: 760000,
    kategori : "Sepatu Sport",
    totalTerjual : 90,
  },
  {
    namaProduct : 'Sepatu Warrior Tristan Black Brown High',
    hargaSatuan: 960000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 37,
  },
  {
    namaProduct : 'Sepatu Warrior Tristan Maroon High ',
    kategori : "Sepatu Sneaker",
    hargaSatuan: 360000,
    totalTerjual : 90,
  },
  {
    namaProduct : 'Sepatu Warrior Rainbow Tosca Corduroy',
    hargaSatuan: 120000,
    kategori : "Sepatu Sneaker",
    totalTerjual : 90,
  }
]

function hitungTotalPenjualan(dataPenjualan){
    if(Array.isArray(dataPenjualan)){
      for(let o=0;o<dataPenjualan.length;o++){
        if(typeof(dataPenjualan)=="object"){
          let total = 0
          for(let i=0;i<dataPenjualan.length;i++){
            total += dataPenjualan[i].totalTerjual
          }
          return total
        }
      }
    }else if(dataPenjualan==null){
      return "Error: Bro where is the parameter?"
    }else{
      return "Error: Invalid data type"
    }
}

console.log(hitungTotalPenjualan(dataPenjualanPakAldi))
//EXPECTED RESULT => 307
//307 dari mana? dari setiap value property 'totalTerjual', yaitu 90 + 37 + 90 + 90