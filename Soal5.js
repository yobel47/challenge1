function getSplitName(personName){
  if(typeof(personName)=="string"){
    let fullName = personName.split(" ")
    switch(fullName.length){
      case 1 :
        return {
          firstName : fullName[0],
          middleName : null,
          lastName : null,
        }
      case 2 :
        return {
          firstName : fullName[0],
          middleName : null,
          lastName : fullName[1]
        }
      case 3 :
        return {
          firstName : fullName[0],
          middleName : fullName[1],
          lastName : fullName[2]
        }
      default :
      	return "ERROR: This function is only for 3 characters name"      	
    }   
  }else if(personName==null){
    return "Error: Bro where is the parameter?"
  }else{
    return "Error: Invalid data type"
  }
}

// EXPECTED RESULT
// Ketika function tersebut dipanggil
console.log(getSplitName("Aldi Daniela Pranata"))
// EXPECTED OUTPUT :
// {firstName: 'Aldi', middleName: 'Daniela', lastName: 'Pranata'}

console.log(getSplitName("Dwi Kuncoro"))
// EXPECTED OUTPUT :
// {firstName: 'Dwi', middleName: null, lastName: 'Kuncoro'}

console.log(getSplitName("Aurora"))
//EXPECTED OUTPUT :
//{firstName: 'Aurora', middleName: null, lastName: null}

console.log(getSplitName("Aurora Aureliya Sukma Darma"))
// EXPECTED OUTPUT :
// Error: This function is only for 3 characters name

console.log(getSplitName(0))
// EXPECTED OUTPUT :
// Error: Invalid data type