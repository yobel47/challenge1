function getAngkaTerbesarKedua(angka){
  if(Array.isArray(angka)){
    let max = Math.max(...angka)
    let sort
 		for(let i=0;i<angka.length;i++){
      sort = angka.sort(function (a, b) {return b - a})
    }
    for(let o=0;o<sort.length;o++){
      if(sort[o]!==max){
        return sort[o]
      }
    }
  }else if(angka==null){
    return "Error: Bro where is the parameter?"
  }else{
    return "Error: Invalid data type"
  }
}

//EXPECTED RESULT
//Ketika Function tersebut dipanggil

const dataAngka = [9,9,4,7,7,4,3,10,2,8]

console.log(getAngkaTerbesarKedua(dataAngka))
// //EXPECTED OUTPUT :
// //8

console.log(getAngkaTerbesarKedua(0))
// //EXPECTED OUTPUT :
// //"ERROR : Tipe data tidak array"

console.log(getAngkaTerbesarKedua())
//EXPECTED OUTPUT :
//"ERROR : Empty"